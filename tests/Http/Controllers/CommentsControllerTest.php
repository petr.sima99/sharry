<?php
declare(strict_types=1);

namespace Tests\Http\Controllers;

use App\Models\Event;
use App\Models\News;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @coversDefaultClass \App\Http\Controllers\CommentsController
 */
final class CommentsControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /**
     * @covers ::createCommentEvent
     */
    public function testCreateCommentEvent(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        /** @var Event $event */
        $event = Event::factory()->create();
        $route = route('events.comments.create', ['event' => $event->id]);

        $response = $this->actingAs($author, 'api')->json('POST', $route, [
            'content' => $this->faker->realText(100),
        ])->assertStatus(Response::HTTP_CREATED);

        $this->assertSame(1, $event->comments()->count());
        $this->assertDatabaseHas('comments', ['user_id' => $author->id]);
        $this->assertDatabaseCount('comments', $event->comments()->count());

        $response->assertJsonStructure([
            'id',
            'content',
            'nick_name',
        ]);
    }

    /**
     * @covers ::createCommentNews
     */
    public function testCreateCommentNews(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        /** @var News $news */
        $news = News::factory()->create();
        $route = route('news.comments.create', ['news' => $news->id]);

        $response = $this->actingAs($author, 'api')->json('POST', $route, [
            'content' => $this->faker->realText(100),
        ])->assertStatus(Response::HTTP_CREATED);

        $this->assertSame(1, $news->comments()->count());
        $this->assertDatabaseHas('comments', ['user_id' => $author->id]);
        $this->assertDatabaseCount('comments', $news->comments()->count());

        $response->assertJsonStructure([
            'id',
            'content',
            'nick_name',
        ]);
    }

    /**
     * @covers ::destroyComment
     */
    public function testDestroyComment(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        /** @var Event $event */
        $event = Event::factory()->create();
        $route = route('events.comments.create', ['event' => $event->id]);

        $newCommentResponse = $this->actingAs($author, 'api')->json('POST', $route, [
            'content' => $this->faker->realText(100),
        ])->assertStatus(Response::HTTP_CREATED);
        $this->assertSame(1, $event->comments()->count());

        $routeDestroy = route('comments.destroy', ['id' => $newCommentResponse->json('id')]);
        $this->actingAs($author, 'api')->json('DELETE', $routeDestroy)->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertSame(0, $event->comments()->count());
    }

    /**
     * @covers ::destroyCommentByNotAuthor
     */
    public function testDestroyCommentWithoutPermission(): void
    {
        /** @var User $author */
        $author = User::factory()->create();
        /** @var User $thief */
        $thief = User::factory()->create();

        /** @var Event $event */
        $event = Event::factory()->create();
        $route = route('events.comments.create', ['event' => $event->id]);

        $newCommentResponse = $this->actingAs($author, 'api')->json('POST', $route, [
            'content' => $this->faker->realText(100),
        ])->assertStatus(Response::HTTP_CREATED);
        $this->assertSame(1, $event->comments()->count());

        $routeDestroy = route('comments.destroy', ['id' => $newCommentResponse->json('id')]);
        $this->actingAs($thief, 'api')->json('DELETE', $routeDestroy)->assertStatus(Response::HTTP_FORBIDDEN);

        $this->assertSame(1, $event->comments()->count());
    }
}
