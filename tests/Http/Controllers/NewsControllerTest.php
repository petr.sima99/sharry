<?php
declare(strict_types=1);

namespace Tests\Http\Controllers;

use App\Models\Comment;
use App\Models\News;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @coversDefaultClass \App\Http\Controllers\NewsController
 */
final class NewsControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /**
     * @covers ::getNewsListWithComments
     */
    public function testGetNewsListWithComments(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        /** @var News $news */
        $news = News::factory()->create();

        /** @var Comment $comment */
        $comment = Comment::factory()->create();
        $comment->user_id = $author->id;
        $news->comments()->save($comment);

        $response = $this->json('GET', route('news.listAll'));

        $response->assertJsonStructure([
            [
                'id',
                'created_at',
                'updated_at',
                'title',
                'content',
                'user_id',
                'comments' => [
                    [
                        'id',
                        'created_at',
                        'updated_at',
                        'content',
                        'user_id',
                    ]
                ]
            ]
        ]);
    }

    /**
     * @covers ::getNewsListWithoutOldOnes
     */
    public function testGetNewsListWithoutOldOnes(): void
    {
        /** @var News $news */
        $news = News::factory()->count(3)->create();

        // Make one news older
        /** @var News $oldNews */
        $oldNews = $news->get(1);
        $oldNews->created_at = Carbon::today()->addDays(-2);
        $oldNews->save();

        $response = $this->json('GET', route('news.listAll'));
        $this->assertSame(2, count($response->json()));
    }

    /**
     * @covers ::createNews
     */
    public function testCreateNews(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        $response = $this->actingAs($author, 'api')->json('POST', route('news.create'), [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->realText(100),
        ])->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('news', ['user_id' => $author->id]);

        $response->assertJsonStructure([
            'id',
            'created_at',
            'updated_at',
            'title',
            'content',
            'user_id',
        ]);
    }

    /**
     * @covers ::updateNews
     */
    public function testUpdateNews(): void
    {
        /** @var User $author */
        $author = User::factory()->create();
        /** @var News $news */
        $news = News::factory()->create();
        $news->user()->associate($author)->save();

        $response = $this->actingAs($author, 'api')->json('PATCH', route('news.update', ['news' => $news->id]), [
            'title' => $newTitle = $this->faker->sentence(),
            'content' => $newContent = $this->faker->realText(100),
        ])->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'id',
            'created_at',
            'updated_at',
            'title',
            'content',
            'user_id',
        ]);

        $this->assertSame($newTitle, $response->json('title'));
        $this->assertSame($newContent, $response->json('content'));
    }

    /**
     * @covers ::destroyNews
     */
    public function testDestroyNews(): void
    {
        /** @var User $author */
        $author = User::factory()->create();
        /** @var News $news */
        $news = News::factory()->create();
        $news->user()->associate($author)->save();

        $route = route('news.destroy', ['news' => $news->id]);
        $this->actingAs($author, 'api')->json('DELETE', $route)->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertDatabaseCount('news', 0);
    }
}
