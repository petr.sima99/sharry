<?php
declare(strict_types=1);

namespace Tests\Http\Controllers;

use App\Models\Comment;
use App\Models\Event;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @coversDefaultClass \App\Http\Controllers\EventsController
 */
final class EventsControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /**
     * @covers ::getEventList
     */
    public function testGetEvents(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        /** @var Event $event */
        $event = Event::factory()->create();
        /** @var Comment $comment */
        $comment = Comment::factory()->create();
        $comment->user_id = $author->id;
        $event->comments()->save($comment);

        $response = $this->actingAs($author, 'api')->json('GET', route('events.listAll'));

        $response->assertJsonStructure([
            [
                'id',
                'created_at',
                'updated_at',
                'valid_from',
                'valid_to',
                'title',
                'content',
                'gps_lat',
                'gps_lng',
                'user_id',
                'comments' => [
                    [
                        'id',
                        'created_at',
                        'updated_at',
                        'content',
                        'user_id',
                    ]
                ]
            ]
        ]);
    }

    /**
     * @covers ::getEventListForSpecificDateTimeRange
     */
    public function testGetEventsByDateTimeRange(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        /** @var Event $event */
        $collection = Event::factory()->count(2)->create();
        /** @var Event $event1 */
        $event1 = $collection->get(0);
        $event1->valid_from = Carbon::today();
        $event1->valid_to = Carbon::today()->addDays(50);
        $event1->save();

        /** @var Event $event2 */
        $event2 = $collection->get(1);
        $event2->valid_from = Carbon::today()->addDays(100);
        $event2->valid_to = Carbon::today()->addDays(150);
        $event2->save();

        $response = $this->actingAs($author, 'api')->json('GET', route('events.listAll', [
            'date_start' => Carbon::today()->addDays(2)->format('Y-m-d'),
            'date_end' => Carbon::today()->addDays(5)->format('Y-m-d')
        ]));

        $this->assertSame(1, count($response->json()));
    }

    /**
     * @covers ::createEvent
     */
    public function testCreateEvent(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        $response = $this->actingAs($author, 'api')->json('POST', route('events.create'), [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->realText(100),
            'valid_from' => Carbon::now()->addDay()->format('d.m.Y'),
            'valid_to' => Carbon::now()->addDays(5)->format('d.m.Y'),
        ])->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('events', ['user_id' => $author->id]);

        $response->assertJsonStructure([
            'id',
            'created_at',
            'updated_at',
            'valid_from',
            'valid_to',
            'title',
            'content',
            'gps_lat',
            'gps_lng',
            'user_id',
        ]);
    }

    /**
     * @covers ::updateEvent
     */
    public function testUpdateEvent(): void
    {
        /** @var User $author */
        $author = User::factory()->create();
        /** @var Event $event */
        $event = Event::factory()->create();
        $event->user()->associate($author)->save();

        $response = $this->actingAs($author, 'api')->json('PATCH', route('events.update', ['event' => $event->id]), [
            'title' => $newTitle = $this->faker->sentence(),
            'content' => $newContent = $this->faker->realText(100),
            'valid_from' => $newValidFrom = Carbon::now()->addDay()->format('d.m.Y'),
            'valid_to' => $newValidTo = Carbon::now()->addDays(5)->format('d.m.Y'),
        ])->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'id',
            'created_at',
            'updated_at',
            'valid_from',
            'valid_to',
            'title',
            'content',
            'gps_lat',
            'gps_lng',
            'user_id',
        ]);

        // All attributes can be tested :)
        $this->assertSame($newTitle, $response->json(['title']));
        $this->assertSame($newContent, $response->json(['content']));
    }

    /**
     * @covers ::updateEventByNoOwner
     */
    public function testUpdateEventByNoOwner(): void
    {
        /** @var User $author */
        $author = User::factory()->create();
        /** @var User $thief */
        $thief = User::factory()->create();
        /** @var Event $event */
        $event = Event::factory()->create();
        $event->user()->associate($author);

        $this->actingAs($thief, 'api')->json('PATCH', route('events.update', ['event' => $event->id]), [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->realText(100),
            'valid_from' => Carbon::now(2),
            'valid_to' => Carbon::now(5),
        ])->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * @covers ::destroyEvent
     */
    public function testDestroyEvent(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        /** @var Event $event */
        $event = Event::factory()->create();
        $event->user()->associate($author)->save();

        $route = route('events.destroy', ['event' => $event->id]);
        $this->actingAs($author, 'api')->json('DELETE', $route)->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertDatabaseCount('events', 0);
    }

    /**
     * @covers ::destroyEventWithCommentNotPossible
     */
    public function testDestroyEventWithCommentNotPosiible(): void
    {
        /** @var User $author */
        $author = User::factory()->create();

        /** @var Event $event */
        $event = Event::factory()->create();
        $event->user()->associate($author)->save();
        /** @var Comment $comment */
        $comment = Comment::factory()->create();
        $event->comments()->attach($comment->id);

        $route = route('events.destroy', ['event' => $event->id]);
        $this->actingAs($author, 'api')->json('DELETE', $route)->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseCount('events', 1);
    }

    /**
     * @covers ::destroyEventByNoAuthor
     */
    public function testDestroyEventByNoAuthor(): void
    {
        /** @var User $author */
        $author = User::factory()->create();
        /** @var User $thief */
        $thief = User::factory()->create();

        /** @var Event $Event */
        $event = Event::factory()->create();
        $event->user()->associate($author)->save();

        $route = route('events.destroy', ['event' => $event->id]);
        $this->actingAs($thief, 'api')->json('DELETE', $route)->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
