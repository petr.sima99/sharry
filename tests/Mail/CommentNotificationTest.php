<?php
declare(strict_types=1);

namespace Tests\Mail;

use App\Mail\CommentCreatedMail;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class CommentNotificationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /**
     * @covers ::sendEmailAfterCommentCreated
     */
    public function testCreateCommentEvent(): void
    {
        // Can be replace by value from DB etc. At the testing environment we do not want to be sent
        // by each test with comments
        config(['notifications' => true]);
        Mail::fake();

        /** @var User $author */
        $author = User::factory()->create();

        /** @var Comment $comment */
        $comment = Comment::factory()->create();
        $comment->user()->associate($author)->save();

        Mail::assertQueued(CommentCreatedMail::class);
    }
}
