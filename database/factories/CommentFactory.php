<?php
declare(strict_types=1);

namespace Database\Factories;

use App\Models\Comment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
     /* @var string */
    protected $model = Comment::class;

    public function definition(): array
    {
        return [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'content' => $this->faker->paragraph(),
            'user_id' => User::factory(),
        ];
    }
}
