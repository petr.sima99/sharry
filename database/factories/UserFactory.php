<?php
declare(strict_types=1);

namespace Database\Factories;

use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /** @var string */
    protected $model = User::class;

    /**
     * @throws BindingResolutionException
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'nick_name' => $this->faker->userName(),
            'email' => $this->faker->safeEmail(),
            'password' => app('hash')->make('1234'),
        ];
    }

    public function unverified(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
