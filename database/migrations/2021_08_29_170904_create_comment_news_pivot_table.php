<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentNewsPivotTable extends Migration
{
    public function up(): void
    {
        Schema::create('comment_news', function (Blueprint $table) {
            $table->unsignedBigInteger('comment_id')->index();
            $table->foreign('comment_id')->references('id')->on('comments')->onDelete('cascade');
            $table->unsignedBigInteger('news_id')->index();
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->primary(['comment_id', 'news_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('comment_news');
    }
}
