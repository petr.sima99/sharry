<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentEventPivotTable extends Migration
{
    public function up(): void
    {
        Schema::create('comment_events', function (Blueprint $table) {
            $table->unsignedBigInteger('comment_id');
            $table->foreign('comment_id')->references('id')->on('comments')->onDelete('cascade');
            $table->unsignedBigInteger('event_id');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->primary(['comment_id', 'event_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('comment_events');
    }
}
