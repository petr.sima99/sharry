@component('mail::message')
    Hello **{{ $body['name'] }}**,  {{-- use double space for line break --}}
    Thank you for choosing Mailtrap!

    Your comment has been added.
    {{ $body['content'] }}

    Sincerely,
    Mailtrap team.
@endcomponent
