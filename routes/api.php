<?php
declare(strict_types=1);

use App\Http\Controllers\CommentsController;
use App\Http\Controllers\EventsController;
use App\Http\Controllers\NewsController;
use Illuminate\Support\Facades\Route;

Route::prefix('/')->group(function () {
    // NewsController - public access
    Route::get('news', [NewsController::class, 'index'])->name('news.listAll');

    Route::middleware('auth:api')->group(function () {

        Route::delete('comments/{id}', [
            CommentsController::class,
            'destroy',
        ])->name('comments.destroy');

        // News
        Route::prefix('news')->group(function () {
            Route::post('/', [
                NewsController::class,
                'create'
            ])->name('news.create');

            Route::patch('{news}', [
                NewsController::class,
                'update'
            ])->name('news.update');

            Route::delete('{news}', [
                NewsController::class,
                'destroy'
            ])->name('news.destroy');

            // Comments
            Route::post('{news}/comments', [
                CommentsController::class,
                'createNews',
            ])->name('news.comments.create');
        });

        // Events
        Route::prefix('events')->group(function () {
            Route::get('/', [
                EventsController::class,
                'index'
            ])->name('events.listAll');

            Route::post('/', [
                EventsController::class,
                'create'
            ])->name('events.create');

            Route::patch('{event}', [
                EventsController::class,
                'update'
            ])->name('events.update');

            Route::delete('{event}', [
                EventsController::class,
                'destroy'
            ])->name('events.destroy');

            // Comments
            Route::post('{event}/comments', [
                CommentsController::class,
                'createEvent',
            ])->name('events.comments.create');
        });
    });
});
