<?php
declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Event
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property ?Carbon $valid_from
 * @property ?Carbon $valid_to
 * @property string $title
 * @property string $content
 * @property string $gps_lat
 * @property string $gps_lng
 * @property int $user_id
 */
class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'valid_from',
        'valid_to',
        'title',
        'content',
        'gps_lat',
        'gps_lng',
    ];

    /** @var string[] */
    protected $with = [
        'user'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function comments(): BelongsToMany
    {
        return $this->belongsToMany(Comment::class, 'comment_events', 'event_id');
    }

    public function isOwner(int $userId): bool
    {
        return $this->user_id === $userId;
    }

    public function hasComments(): bool
    {
        return $this->comments()->count() > 0;
    }
}
