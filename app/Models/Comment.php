<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $nick_name
 * @property string $content
 * @property int $user_id
 * @property-read User $user
 */
class Comment extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $fillable = [
        'nick_name',
        'content',
    ];

    /** @var string[] */
    protected $with = [
        'user'
    ];

    public function getNickNameAttribute(): string
    {
        return $this->user->nick_name;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function isOwner(int $userId): bool
    {
        return $this->user_id === $userId;
    }
}
