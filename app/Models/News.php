<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $title
 * @property string $content
 * @property int $user_id
 * @package App\Models
 */
class News extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'content',
        'title',
        'content',
        'gps_lat',
        'gps_lng',
    ];

    /** @var string[] */
    protected $with = [
        'user'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function comments(): BelongsToMany
    {
        return $this->belongsToMany(Comment::class, 'comment_news', 'news_id');
    }

    public function isOwner(int $userId): bool
    {
        return $this->user_id === $userId;
    }

    public function hasComments(): bool
    {
        return $this->comments()->count() > 0;
    }
}
