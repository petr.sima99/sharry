<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\Comments\CommentCreateForEventRequest;
use App\Http\Requests\Comments\CommentCreateForNewsRequest;
use App\Http\Requests\Comments\CommentDestroyRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CommentsController extends Controller
{
    public function createEvent(CommentCreateForEventRequest $request): JsonResponse
    {
        $comment = new Comment();
        $comment->content = $request->getContentText();
        $comment->user()->associate($request->getAuthor());

        $request->getEvent()->comments()->save($comment);

        return new JsonResponse(
            new CommentResource($comment),
            Response::HTTP_CREATED
        );
    }

    public function createNews(CommentCreateForNewsRequest $request): JsonResponse
    {
        $comment = new Comment();
        $comment->content = $request->getContentText();
        $comment->user()->associate($request->getAuthor());

        $request->getNews()->comments()->save($comment);

        return new JsonResponse(
            new CommentResource($comment),
            Response::HTTP_CREATED
        );
    }

    public function destroy(CommentDestroyRequest $request): JsonResponse
    {
        Comment::findOrFail($request->getId())->delete();

        return new JsonResponse(
            [],
            Response::HTTP_NO_CONTENT
        );
    }
}
