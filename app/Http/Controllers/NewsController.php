<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\News\NewsCreateRequest;
use App\Http\Requests\News\NewsDestroyRequest;
use App\Http\Requests\News\NewsListAllRequest;
use App\Http\Requests\News\NewsUpdateRequest;
use App\Http\Resources\NewsResource;
use App\Models\News;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends Controller
{
    public function index(NewsListAllRequest $request): JsonResponse
    {
        $collection = News::with('comments')
            ->whereDate('created_at', '>=', Carbon::today()->toDateTimeString())
            ->orderBy('created_at')
            ->get();

        return new JsonResponse(NewsResource::collection($collection));
    }

    public function create(NewsCreateRequest $request): JsonResponse
    {
        $newObject = new News();
        $newObject->title = $request->getTitle();
        $newObject->content = $request->getContentText();
        $newObject->user()->associate($request->getAuthor())->save();

        return new JsonResponse(
            NewsResource::make($newObject),
            Response::HTTP_CREATED,
        );
    }

    public function update(NewsUpdateRequest $request): JsonResponse
    {
        /** @var News $news */
        $news = News::findOrFail($request->getId());
        $news->title = $request->getTitle();
        $news->content = $request->getContentText();
        $news->save();

        return new JsonResponse(NewsResource::make($news));
    }

    public function destroy(NewsDestroyRequest $request): JsonResponse
    {
        News::findOrFail($request->getId())->delete();

        return new JsonResponse(
            [],
            Response::HTTP_NO_CONTENT
        );
    }
}
