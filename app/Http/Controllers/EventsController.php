<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\Events\EventCreateRequest;
use App\Http\Requests\Events\EventDestroyRequest;
use App\Http\Requests\Events\EventsListAllRequest;
use App\Http\Requests\Events\EventUpdateRequest;
use App\Http\Resources\EventsResource;
use App\Models\Event;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class EventsController extends Controller
{
    public function index(EventsListAllRequest $request): JsonResponse
    {
        $collection = Event::with('comments')
            ->when($request->getStartDate(), function (Builder $builder) use ($request) {
                $builder->whereDate('valid_from', '>=', $request->getStartDate());
            })
            ->when($request->getEndDate(), function (Builder $builder) use ($request) {
                $builder->orWhereBetween('valid_to', [$request->getStartDate(), $request->getEndDate()]);
            })
            ->orderBy('valid_from')
            ->get();

        return new JsonResponse(EventsResource::collection($collection));
    }

    public function create(EventCreateRequest $request): JsonResponse
    {
        $newEvent = new Event();
        $newEvent->title = $request->getTitle();
        $newEvent->content = $request->getContentText();
        $newEvent->valid_from = $request->getValidFrom();
        $newEvent->valid_to = $request->getValidTo();
        $newEvent->gps_lat = $request->getGpsLat();
        $newEvent->gps_lng = $request->getGpsLng();
        $newEvent->user()->associate($request->getAuthor())->save();

        return new JsonResponse(
            EventsResource::make($newEvent),
            Response::HTTP_CREATED
        );
    }

    public function update(EventUpdateRequest $request): JsonResponse
    {
        /** @var Event $event */
        $event = Event::findOrFail($request->getId());
        $event->title = $request->getTitle();
        $event->content = $request->getContentText();
        $event->valid_from = $request->getValidFrom();
        $event->valid_to = $request->getValidTo();
        $event->gps_lat = $request->getGpsLat();
        $event->gps_lng = $request->getGpsLng();
        $event->save();

        return new JsonResponse(EventsResource::make($event));
    }

    public function destroy(EventDestroyRequest $request): JsonResponse
    {
        Event::findOrFail($request->getId())->delete();

        return new JsonResponse(
            [],
            Response::HTTP_NO_CONTENT
        );
    }
}
