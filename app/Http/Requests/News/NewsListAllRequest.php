<?php
declare(strict_types=1);

namespace App\Http\Requests\News;;

use Illuminate\Foundation\Http\FormRequest;

class NewsListAllRequest extends FormRequest
{
    public function rules(): array
    {
        return [];
    }
}
