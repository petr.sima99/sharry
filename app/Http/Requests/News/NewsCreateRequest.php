<?php

namespace App\Http\Requests\News;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class NewsCreateRequest extends FormRequest
{
    public function getAuthor(): User
    {
        return $this->user('api');
    }

    public function getTitle(): string
    {
        return (string)Arr::get($this->validated(), 'title');
    }

    public function getContentText(): string
    {
        return (string)Arr::get($this->validated(), 'content');
    }

    public function rules(): array
    {
        return [
            'title' => ['required', 'max:128'],
            'content'=> ['required', 'max:500'],
        ];
    }
}
