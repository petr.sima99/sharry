<?php
declare(strict_types=1);

namespace App\Http\Requests\News;;

use App\Models\News;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

/**
 * @property-read News $news
 */
class NewsUpdateRequest extends FormRequest
{
    public function getAuthor(): User
    {
        return $this->user('api');
    }

    public function getTitle(): ?string
    {
        $validated = Arr::get($this->validated(), 'title');

        return $validated ?? $this->news->title;
    }

    public function getContentText(): ?string
    {
        $validated = Arr::get($this->validated(), 'content');

        return $validated ?? $this->news->content;
    }

    public function getId(): int
    {
        return $this->news->id;
    }

    public function authorize(): bool
    {
        return $this->news->isOwner($this->getAuthor()->id);
    }

    public function rules(): array
    {
        return [
            'title' => 'max:128',
            'content'=> 'max:500',
        ];
    }
}
