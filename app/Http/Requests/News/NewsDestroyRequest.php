<?php
declare(strict_types=1);

namespace App\Http\Requests\News;;

use App\Models\News;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

/**
 * @property-read News $news
 */
class NewsDestroyRequest extends FormRequest
{
    private function getAuthor(): User
    {
        return $this->user('api');
    }

    public function rules(): array
    {
        return [];
    }

    public function authorize(): bool
    {
        return $this->news->isOwner($this->getAuthor()->id) && $this->news->hasComments() === false;
    }

    public function getId(): int
    {
        return $this->news->id;
    }
}
