<?php
declare(strict_types=1);

namespace App\Http\Requests\Comments;


use App\Models\News;

/**
 * @property-read News $news
 */
class CommentCreateForNewsRequest extends CommentCreateRequest
{
    public function getNews(): News
    {
        return $this->news;
    }
}
