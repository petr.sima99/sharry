<?php
declare(strict_types=1);

namespace App\Http\Requests\Comments;

use App\Models\Event;

/**
 * @property-read Event $event
 */
class CommentCreateForEventRequest extends CommentCreateRequest
{
    public function getEvent(): Event
    {
        return $this->event;
    }
}
