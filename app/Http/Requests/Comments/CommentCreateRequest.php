<?php
declare(strict_types=1);

namespace App\Http\Requests\Comments;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class CommentCreateRequest extends FormRequest
{
    public function getAuthor(): User
    {
        return $this->user('api');
    }

    public function getContentText(): string
    {
        return (string) Arr::get($this->validated(), 'content');
    }

    public function rules(): array
    {
        return [
            'content' => ['required', 'string', 'min:1', 'max:500'],
        ];
    }
}
