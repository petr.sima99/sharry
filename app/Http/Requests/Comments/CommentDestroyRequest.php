<?php
declare(strict_types=1);

namespace App\Http\Requests\Comments;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class CommentDestroyRequest extends FormRequest
{
    public function getAuthor(): User
    {
        return $this->user('api');
    }

    public function getId(): int
    {
        return (int)$this->route('id');
    }

    public function authorize(): bool
    {
        return (Comment::find($this->getId()))?->isOwner($this->getAuthor()->id);
    }

    public function rules(): array
    {
        return [];
    }
}
