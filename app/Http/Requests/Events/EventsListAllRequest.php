<?php
declare(strict_types=1);

namespace App\Http\Requests\Events;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class EventsListAllRequest extends FormRequest
{
    public function getStartDate(): ?Carbon
    {
        $value = Arr::get($this->validated(), 'date_start');

        return $value ? Carbon::parse($value) : $value;
    }

    public function getEndDate(): ?Carbon
    {
        $value = Arr::get($this->validated(), 'date_end');

        return $value ? Carbon::parse($value) : $value;
    }

    public function rules(): array
    {
        return [
            'date_start' => ['date', 'required_with:date_end'],
            'date_end' => ['date', 'after:start'],
        ];
    }
}
