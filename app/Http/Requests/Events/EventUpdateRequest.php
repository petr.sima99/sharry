<?php
declare(strict_types=1);

namespace App\Http\Requests\Events;

use App\Models\Event;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

/**
 * @property-read Event $event
 */
class EventUpdateRequest extends FormRequest
{
    public function getAuthor(): User
    {
        return $this->user('api');
    }

    public function getId(): int
    {
        return $this->event->id;
    }

    public function getTitle(): ?string
    {
        $validated = Arr::get($this->validated(), 'title');

        return $validated ?? $this->event->title;
    }

    public function getContentText(): ?string
    {
        $validated = Arr::get($this->validated(), 'content');

        return $validated ?? $this->event->content;
    }

    public function getValidFrom(): Carbon
    {
        $validated = Arr::get($this->validated(), 'valid_from');

        return $validated ? Carbon::parse($validated) : $this->event->valid_from;
    }

    public function getValidTo(): Carbon
    {
        $validated = Arr::get($this->validated(), 'valid_to');

        return $validated ? Carbon::parse($validated) : $this->event->valid_from;
    }

    public function authorize(): bool
    {
        return $this->event->isOwner($this->getAuthor()->id);
    }

    public function getGpsLat(): ?string
    {
        return Arr::get($this->validated(), 'gps_lat');
    }

    public function getGpsLng(): ?string
    {
        return Arr::get($this->validated(), 'gps_lng');
    }

    public function rules(): array
    {
        return [
            'title' => 'max:128',
            'content'=> 'max:500',
            'valid_from' => ['date', 'after:today'],
            'valid_to' => ['date', 'after:valid_from'],
            'gps_lat' => ['nullable', 'string'],
            'gps_lng' => ['nullable', 'string'],
        ];
    }
}
