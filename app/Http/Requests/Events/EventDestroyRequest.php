<?php
declare(strict_types=1);

namespace App\Http\Requests\Events;

use App\Models\Event;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property-read Event $event
 */
class EventDestroyRequest extends FormRequest
{
    private function getAuthor(): User
    {
        return $this->user('api');
    }

    public function rules(): array
    {
        return [];
    }

    public function authorize(): bool
    {
        return $this->event->isOwner($this->getAuthor()->id) && $this->event->hasComments() === false;
    }

    public function getId(): int
    {
        return $this->route('event')->id;
    }
}
