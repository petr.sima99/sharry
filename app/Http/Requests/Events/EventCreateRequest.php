<?php
declare(strict_types=1);

namespace App\Http\Requests\Events;

use App\Models\Event;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

/**
 * @property-read Event $event
 */
class EventCreateRequest extends FormRequest
{
    public function getAuthor(): User
    {
        return $this->user('api');
    }

    public function getTitle(): ?string
    {
        return Arr::get($this->validated(), 'title');
    }

    public function getContentText(): ?string
    {
        return Arr::get($this->validated(), 'content');
    }

    public function getValidFrom(): Carbon
    {
        return Carbon::parse(Arr::get($this->validated(), 'valid_from'));
    }

    public function getValidTo(): Carbon
    {
        return Carbon::parse(Arr::get($this->validated(), 'valid_to'));
    }

    public function getGpsLat(): ?string
    {
        return Arr::get($this->validated(), 'gps_lat');
    }

    public function getGpsLng(): ?string
    {
        return Arr::get($this->validated(), 'gps_lng');
    }

    public function rules(): array
    {
        return [
            'title' => ['required', 'max:128'],
            'content'=> ['required', 'max:500'],
            'valid_from' => ['required', 'date', 'after:today'],
            'valid_to' => ['required', 'date', 'after:valid_from'],
            'gps_lat' => ['nullable', 'string'],
            'gps_lng' => ['nullable', 'string'],
        ];
    }
}
