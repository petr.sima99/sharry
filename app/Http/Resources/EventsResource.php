<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\Event;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

/**
 * @property-read Event $resource
 */
class EventsResource extends JsonResource
{
    public function toArray($request): array
    {
        return Arr::only(
            parent::toArray($request),
            [
                'id',
                'created_at',
                'updated_at',
                'valid_from',
                'valid_to',
                'title',
                'content',
                'gps_lat',
                'gps_lng',
                'user_id',
                'comments',
            ]
        );
    }
}
