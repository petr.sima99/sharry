<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\News;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

/**
 * @property-read News $resource
 */
class NewsResource extends JsonResource
{
    public function toArray($request): array
    {
        return Arr::only(
            parent::toArray($request),
            [
                'id',
                'created_at',
                'updated_at',
                'title',
                'content',
                'user_id',
            ]
        );
    }
}
