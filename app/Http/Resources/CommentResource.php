<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\Comment;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

/**
 * @property-read Comment $resource
 */
class CommentResource extends JsonResource
{
    public function __construct(Comment $resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request): array
    {
        $data = parent::toArray($request);
        $data['nick_name'] = $this->resource->nick_name;

        return Arr::only(
            $data,
            [
                'id',
                'created_at',
                'updated_at',
                'content',
                'user_id',
                'nick_name',
            ]
        );
    }
}
