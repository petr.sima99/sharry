<?php
declare(strict_types=1);

namespace App\Observers;

use App\Mail\CommentCreatedMail;
use App\Models\Comment;
use Illuminate\Support\Facades\Mail;

class CommentObserver
{
    public function created(Comment $comment): void
    {
        // Dont run notifications everytime
        if (!config('notifications', false)) {
            return;
        }

        $user = $comment->user;
        $body = [
            'content' => $comment->content,
            'name' => $user->nick_name,
        ];

        Mail::to($user)->send(new CommentCreatedMail($body));
    }
}
